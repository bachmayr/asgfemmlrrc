# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis

#=required:
DyadicIndexType
NodeIndexType
j(DyadicIndexType) - Level
k() - comparable index, using < within one level, order relation should be preserved for children
children(idx )
ancestor
getDomain
common_ancestor
geometric_next - for iterator
=#


#is always assumed to be disjoint and may be assumed to be a covering (of the entire unit line [0,1]) by some routines.

#tree with inner nodes as keys and (a vector of) children as values - e.g. empty tree is only the root
#keys are inner nodes, leaves are the only nodes that are only children (of some key) and not
const treeDict = Dict{DyadicIndexType, Vector{DyadicIndexType}}
const Tiling = Vector{DyadicIndexType}
const Mesh = Vector{DyadicIndexType} #Vector of sorted (left to right in tree) tiles, that discretise the entire domain
const SubMesh = Vector{DyadicIndexType} #Vector of sorted (left to right in tree) tiles, that discretise only part of the getDomain
const MeshDict = Matrix{Int64} # a dictionary for indexes column is the tile and row is the local index
const SmartMesh = Tuple{Mesh, MeshDict, Int64, treeDict, Dict{DyadicIndexType, Int64}} #Mesh, Index for DoFs, Fe Dim, tree for the mesh, map leaves -> index in mesh

#starting from here, the routines should not depend on the spatial dimension and work depending on a proper implementation 
# of rourines like children, ancestor, 
function isRoot(idx::DyadicIndexType)
    return j(idx) == 0
end

function children_abs(idx::DyadicIndexType, l::Integer)
    @assert (j(idx)≤l)
    return children(idx,l-j(idx))
end

function children(tiles::Tiling, l::Integer=1)
    return reduce(vcat, [children(tile, l) for tile in tiles])
end

function ancestor_abs(idx::DyadicIndexType,l::Integer)
    @assert (j(idx)≥l)
    return ancestor(idx,j(idx)-l)
end

function intersect(tiles::Tiling, tile2::DyadicIndexType)
    ans = false
    for tile1 in tiles
        if intersect(tile1,tile2)
            ans = true
        end
    end
    return ans
end
function intersection(tile1::DyadicIndexType,tile2::DyadicIndexType)
    @assert intersect(tile1, tile2)
    return j(tile1) > j(tile2) ? tile1 : tile2
end

function strictlylessintree(tile1::DyadicIndexType, tile2::DyadicIndexType)
    anc1 = deepcopy(tile1)
    anc2 = deepcopy(tile2)
    if j(anc1) ≠ j(anc2)
        level = min(j(tile1),j(tile2))
        anc1 = ancestor_abs(tile1,level)
        anc2 = ancestor_abs(tile2,level)
    end
    simblings = j(anc1)==0 ? StaticRoots() : children(ancestor(anc1))
    while anc2 ∉ simblings
        anc1 = ancestor(anc1)
        anc2 = ancestor(anc2)
        simblings = j(anc1)==0 ? StaticRoots() : children(ancestor(anc1))
    end
    return indexin(anc1,simblings) < indexin(anc2,simblings)

end

function CumulativeVals!(Vals::Dict{GlobalFE1D,Float64})
	#first sort tiles into level buckets
	levelFEs = Vector{Vector{GlobalFE1D}}()
	for fe in keys(Vals)
		while length(levelFEs) ≤ fe[1]
			push!(levelFEs,Vector{GlobalFE1D}())
		end
		push!(levelFEs[fe[1]], fe) 
	end
    levelFEs[1] = StaticFiniteElementsRoots()
	for l in length(levelFEs):-1:2
		for fe in levelFEs[l]
			a = ancestorframe(fe)
            if haskey(Vals,a)
                Vals[a] += Vals[fe]
            else
                Vals[a] = Vals[fe]
            end
		end	
	end
end

function CumulativeValsforTiles!(Vals::Dict{DyadicIndex1D,Float64})
	#first sort tiles into level buckets
	levelTiles = Vector{Vector{DyadicIndex1D}}()
	for tile in keys(Vals)
		while length(levelTiles) ≤ tile[1]
			push!(levelTiles,Vector{DyadicIndex1D}())
		end
        if tile[1] > 0
		    push!(levelTiles[tile[1]], tile) 
        end
	end
    Vals[(0,0)] = 0.0
	for l in length(levelTiles):-1:1
		for tile in levelTiles[l]
			a = ancestor(tile)
            if haskey(Vals,a)
                Vals[a] += Vals[tile]
            else
                Vals[a] = Vals[tile]
            end
		end	
	end
end

function getMeshFromBPXwithTileTree(Fext, C::Vector{Dict{GlobalFE1D,Float64}}, leaves::Vector{Tuple{Int64,DyadicIndex1D}} , c_th::Float64)
    Ctiles = [Dict{DyadicIndex1D,Float64}() for c in C]
    for i in 1:length(C)
        for (fe,val) in C[i]
            for tile in supportTiles(fe)
                if haskey(Ctiles[i], tile)
                    Ctiles[i][tile] += val^2
                else
                    Ctiles[i][tile] = val^2
                end
            end
        end
    end
    for (i,tile) in leaves
        if !haskey(Ctiles[i],tile)
            Ctiles[i][tile] = 0.0
        end
    end

    CumulativeValsforTiles!.(Ctiles)

    leaves = thresholding( Ctiles, leaves, c_th)

    idxes = [tile[1] == 0 ? 0 : i for (i,tile) in leaves]  # write Fext-index if there is some refinement for this index.
	unique!(idxes)
	sort!(idxes)
	if idxes[1]==0
		deleteat!(idxes,1)
	end
    #idxes now contains the sub-list of all Fext we want to keep

	S = Vector{Mesh}(undef, length(idxes))
	for i in 1:length(S)
		S[i] = Mesh()
	end

	idxinv = Dict([j => i for (i,j) in enumerate(idxes)])
	for (i,tile) in leaves
		if haskey(idxinv,i)
			push!(S[idxinv[i]],tile)
		end
	end

    for i in 1:length(S)
        S[i] = getMesh(makeMinTree(S[i]))
	end

    if GradeMesh
        S = gradeMesh.(S)
    end

	newleaves = Vector{Tuple{Int64,DyadicIndex1D}}()
	for (i,tile) in leaves
		if haskey(idxinv,i)
			push!(newleaves,(idxinv[i],tile))
		end
	end

    return S, MultiIndexMap([Fext[i] for i in idxes]), newleaves


end


function getMeshFromBPX(Fext, C::Vector{Dict{GlobalFE1D,Float64}}, leaves::Vector{Tuple{Int64,GlobalFE1D}} , c_th::Float64)
    
    for i in 1:length(C)
        for (fe,val) in C[i]
            C[i][fe] = val^2
        end
    end
    for (i,fe) in leaves
        if !haskey(C[i],fe)
            C[i][fe] = 0.0
        end
    end
    CumulativeVals!.(C)
    leaves = thresholding( C, leaves, c_th)

    idxes = [fe[1] == 1 ? 0 : i for (i,fe) in leaves]  # write Fext-index if there is some refinement for this index.
	unique!(idxes)
	sort!(idxes)
	if idxes[1]==0
		deleteat!(idxes,1)
	end
	#idxes now contains the sub-list of all Fext we want to keep

	S = Vector{Mesh}(undef, length(idxes))
	for i in 1:length(S)
		S[i] = Mesh()
	end

	idxinv = Dict([j => i for (i,j) in enumerate(idxes)])
	for (i,fe) in leaves
		if haskey(idxinv,i)
			append!(S[idxinv[i]],supportTiles(fe))
		end
	end

	for i in 1:length(S)
        S[i] = getMesh(makeMinTree(S[i]))
	end
    if GradeMesh
        S = gradeMesh.(S)
    end

	newleaves = Vector{Tuple{Int64,GlobalFE1D}}()
	for (i,fe) in leaves
		if haskey(idxinv,i)
			push!(newleaves,(idxinv[i],fe))
		end
	end

    return S, MultiIndexMap([Fext[i] for i in idxes]), newleaves


end


function refine(tiling::Tiling, mark::Vector{Bool},l::Integer=1)
    if l>0
        len = (length(mark) - count(mark))+count(mark)*2^(l)
        refined = Tiling(undef,len)
        i = 0
        for (flag,idx) in zip(mark,tiling)
            if flag
                children_i = children(idx,l)
                len_i = length(children_i)
                refined[i+1:i+len_i] = children_i
                i += len_i
            else
                i += 1
                refined[i] = idx
            end
        end
        return refined
    elseif l==0
        return deepcopy(tiling)
    else
        throw("negative refinement")
    end
end



function refine(tiling::Tiling,l::Integer=1)
    return refine(tiling,[true for t in tiling],l)
end

#makes a tree from leaves, only for disjoint tiles
function makeTree(tiles::Tiling)
    tree = treeDict()
    while length(tiles) > 0
        deleteat!(tiles, [isRoot(t) for t in tiles])
        for t in tiles
            a = ancestor(t)
            if haskey(tree,a)
                if t ∉ tree[a]
                    push!(tree[a],t)
                end
            else
                tree[a] = [t]
            end
        end
        tiles = ancestor.(tiles)
        unique!(tiles)
    end
    return tree
end


#makes minimal tree from set of tiles
function makeMinTree(tiles::Tiling)
    tree = treeDict()
    while length(tiles) > 0
        deleteat!(tiles, [isRoot(t) for t in tiles])
        for t in tiles
            a = ancestor(t)
            if haskey(tree,a)
                if t ∉ tree[a]
                    push!(tree[a],t)
                end
            else
                tree[a] = [t]
            end
        end
        tiles = ancestor.(tiles)
        unique!(tiles)
    end
    return tree
end

function gradeMesh(tiles::Tiling)
    difflevel = 1
    notdone = true
    while notdone
        marker = [false for t in tiles]
        for i in 2:length(tiles)
            if j(tiles[i]) + difflevel < j(tiles[i-1] )
                marker[i] = true
            end
        end
        for i in 1:length(tiles)-1
            if j(tiles[i]) + difflevel < j(tiles[i+1] )
                marker[i] = true
            end
        end
        tiles = refine(tiles,marker)
        notdone = any(marker)
    end
    return tiles
end

#makes minimal tree from set of tiles and sums the values on children

function removeLeaves(tree::treeDict)
    return makeMinTree(collect(keys(tree)))
end

function makeCumulativeTree(tiles::Tiling, vals::Dict{DyadicIndexType,Float64})
    tree = makeMinTree(tiles)
    return makeCumulativeTree(tree, vals)
end

function makeCumulativeTree(tree::Dict{DyadicIndexType,Vector{DyadicIndexType}}, vals::Dict{DyadicIndexType,Float64})
    valtree = deepcopy(vals)
    while length(tree) > 0          #iterate through the tree from leaves to root
        leaves = getMesh(tree)
        for node in leaves
            a = ancestor(node)
            if haskey(valtree, a)
                valtree[a] += valtree[node]
            else
                valtree[a] = valtree[node]
            end
        end
        tree = removeLeaves(tree)
    end
    return valtree
end

#get leaves as SubMesh from subtree starting at a node
function getLeaves(tree::treeDict, nodes::Vector{DyadicIndexType})
    @assert reduce(&, [isRoot(node) || haskey(tree, ancestor(node)) for node in nodes] )
    stack = deepcopy(nodes)
    submesh = SubMesh()
    while length(stack) > 0 
        s = pop!(stack)
        if haskey(tree,s)
            append!(stack, reverse(childrenTuple(s)) )
        else
            push!(submesh,s)
        end
    end
    return submesh
end

function getLeaves(tree::treeDict, node::DyadicIndexType)
    getLeaves(tree, [node])
end

function findIntersection(tree::treeDict, node::DyadicIndexType)
    if isRoot(node) || haskey(tree, ancestor(node)) 
        return getLeaves(tree, node)
    else
        anc = deepcopy(node)
        while !(isRoot(anc) || haskey(tree, ancestor(anc)) )
            anc = ancestor(anc)
        end
        if haskey(tree, anc) && intersect(node,tree[anc])
            return [tree[anc]]
        else
            return [node][1:0]
        end
    end
end




#get mesh from tree
function getMesh(tree::treeDict)
    getLeaves(tree, reverse(StaticRoots()) )
end

function getMesh(tiles::Tiling)
    getMesh( makeTree(tiles) )
end


function height(tree::treeDict)
    return max(0, maximum([v[1][1] for (k,v) in tree]))+1
end
#get levelwise submeshes from tree
function getSubMeshes(tree::treeDict)

    stack = StaticRoots()
    submeshes = Vector{SubMesh}(undef, height(tree))
    for i in 1:length(submeshes) 
        submeshes[i] = SubMesh()
    end
    while length(stack) > 0 
        s = pop!(stack)
        if haskey(tree,s)
            append!(stack, reverse!(children(s)) )
        end
        push!(submeshes[ s[1]+1 ],s)
    end
    return submeshes
end

function makeTree(pp::piecewisePoly)
    tiles = collect(keys(pp))
    return makeTree(tiles)
end

function mergeTrees(trees::Vector{treeDict})
    tree = treeDict()
    tiles = StaticRoots()
    while length(tiles) > 0
        for tile in tiles, t in trees
            #add new children
            if haskey(tree,tile) && haskey(t,tile)
                for newtile in t[tile]
                    if newtile ∉ tree[tile]
                        push!(tree[tile],newtile)
                    end
                end
            elseif haskey(t,tile)
                tree[tile] = t[tile]
            end
        end
        # get next level candidates
        # look at children on elements of tiles, over all trees
      @timeit to "reduce 2" begin
        newtiles = Set{DyadicIndexType}()
        for tile in tiles, t in trees
            if haskey(t, tile)
                for el in t[tile]
                    push!(newtiles, el)
                end
            end
        end 
        tiles = newtiles
      end
    end
    return tree
end





