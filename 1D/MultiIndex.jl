# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


const MultiIndex = Vector{Int64}

function encode(vec::MultiIndex)
    ν = MultiIndex()
    for i in vec
        if i != 0
            push!(ν,i)
        else
            if get(ν,length(ν),1) > 0
                push!(ν,-1)
            else
                ν[end] -= 1
            end
        end
    end
    if get(ν,length(ν),1) < 0
        pop!(ν)
    end
    return ν
end

function decode(ν::MultiIndex)
    vec = Vector{Int64}()
    for n in ν
        if n > 0
            push!(vec,n)
        else
            append!(vec,zeros(Int64,-n))
        end
    end
    return vec
end


function getValue(ν::MultiIndex, i::Int64)
    pos = 0
    j = 0
    while pos < i && j < length(ν)
        j += 1
        if ν[j] < 0
            pos = pos - ν[j]
        else
            pos = pos + 1
        end
    end

    if pos == i && ν[j]>0    #index found
        return ν[j]
    else            #index not found
        return 0
    end
end

function inc!(ν_new::MultiIndex, ν::MultiIndex, i::Int64)
    pos = 0
    j = 0
    while j < length(ν) && pos < i
        j += 1
        if ν[j] < 0
            pos = pos - ν[j]
        else
            pos = pos + 1
        end
    end
    new_len = length(ν) - 1
    j_offset = 0
    if pos == i && ν[j] > 0
        new_len += 1
        resize!(ν_new,new_len)
        ν_new[j]=ν[j]+1
    else #pos >= i, old pos = pos + ν[j]
        if j == 0 || ν[j] > 0 #past the end on ν
            pos_old = pos
            pos = i
            new_len += 1
            j_offset += 1
        else
            pos_old = pos + ν[j] # <i
        end

        if pos_old - i + 1 < 0
            new_len += 1
        end
        new_len += 1
        if i - pos < 0
            new_len += 1
        end
        resize!(ν_new,new_len)
        if j > 0 && ν[j] > 0 #past the end on ν
            ν_new[j]=ν[j]
        end

        if pos_old - i + 1 < 0
            ν_new[j+j_offset] = pos_old - i + 1 # < 0
            j_offset += 1
        end
        ν_new[j+j_offset]=1
        if i - pos < 0
            j_offset += 1
            ν_new[j+j_offset] = i-pos
        end
    end
    ν_new[1:j-1]=ν[1:j-1]
    ν_new[j+j_offset+1:length(ν)+j_offset] = ν[j+1:length(ν)]
    return ν_new
end

function dec!(ν_new::MultiIndex, ν::MultiIndex, i::Int64)
    pos = 0
    j = 0
    while j < length(ν) && pos < i
        j += 1
        if ν[j] < 0
            pos = pos - ν[j]
        else
            pos = pos + 1
        end
    end
    new_len = length(ν)
    j_offset = 0
    if pos == i && ν[j] > 0
        if ν[j] > 1
            resize!(ν_new,new_len)
            ν_new[j]=ν[j]-1
        else                                #reduce a 1
            gap = 1
            j_offset = 0
            if j > 1 && ν[j-1] < 0          #gap before 1
                gap -= ν[j-1]
                new_len -= 1
                j_offset -= 1
            end
            if j < length(ν) && ν[j+1] < 0  #gap after 1
                gap -= ν[j+1]
                new_len -= 1
                j_offset -= 1
                j += 1
            end
            if j + j_offset == new_len
                new_len -= 1                #don't write negative number at the end
                resize!(ν_new,new_len)
            else
                resize!(ν_new,new_len)
                ν_new[j + j_offset]  = -gap
            end
        end
        ν_new[1:j+j_offset-1]=ν[1:j+j_offset-1]
        ν_new[j+j_offset+1:length(ν)+j_offset] = ν[j+1:length(ν)]
        return true
    else
        return false
    end
end

function dim(ν::MultiIndex)
    d = 0
    for n in ν
        if n > 0
            d += 1
        else
            d -= n
        end
    end
    return d
end


struct MultiIndexBasis
    i::Int64
end

import Base.+
function +(ν::MultiIndex, e::MultiIndexBasis)
    ν_new = MultiIndex()
    inc!(ν_new,ν,e.i)
    return ν_new
end

import Base.-
function -(ν::MultiIndex, e::MultiIndexBasis)
    ν_new = MultiIndex()
    if !dec!(ν_new,ν,e.i)
        throw("dec! failed")
    end
    return ν_new
end
