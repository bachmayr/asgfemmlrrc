# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis

#piecewise polynomials on trees

function feToPP(mesh::Mesh, u::Vector{Float64})
    Deg = localDGDim()-1
    meshdict = meshDict(mesh)
    return Dict([mesh[i] => [ val(meshdict, mesh, i, u, x ) for x in xGrid(mesh[i], Deg) ]   for i in 1:length(mesh)]  )
end



import Base.*
*(a::Float64,v::piecewisePoly) #=where d =#= (return Dict([k => a*p for (k,p) in v]))




function piecewisePolyToDiscontinuousGalerkin(pp::piecewisePoly)
    roots = StaticRoots()
    tree = Set(roots)
    sizehint!(tree,2*length(pp))
    for t in roots
        if !haskey(pp, t)
            pp[t] = (0.,0.)
        end
    end
    tiles = deepcopy(keys(pp))
    #build tree and initialize sum as zero
    for tile in tiles 
        a = tile
        if tile[1]>0
            a = ancestor(tile)
        end
        while !(a in tree) && !(tile in tree)
            push!(tree, a)
            if !haskey(pp,a)
                pp[a] = (0.,0.)
            end
            a = ancestor(a)  
        end
    end
    tiles = roots
    #add by iterating through all relevant tiles and through tree
    while length(tiles) > 0 
        for t in tiles
            hk = t in tree 
            if hk
                coeffs = pp[t]
                delete!(pp,t)
                for child in childrenTuple(t) 
                    b = translateTileValues(t, coeffs, child)
                    if haskey(pp, child)
                        pp[child] = pp[child] .+  b
                    else
                        pp[child] = b
                    end
                end
            end
        end
        newtiles = Vector{DyadicIndex1D}()
        for tile in tiles
            if tile in tree
                append!(newtiles, childrenTuple(tile))
            end
        end
        tiles = newtiles 
    end
    #use other format for output to match later algorithm
    tiling = Vector{DyadicIndex1D}()
    discontinuousg = Vector{Tuple{Float64,Float64}}()
    stack = reverse!(StaticRoots())
    while length(stack) > 0 
        tile = pop!(stack)
        if tile in tree
            append!(stack, reverse(childrenTuple(tile)))
        elseif haskey(pp, tile)
            push!(tiling, tile)
            push!(discontinuousg, pp[tile])
        end
    end
    return (tiling, discontinuousg)
end


function discontinuousGalerkinToVector(rowmesh::Vector{DyadicIndexType}, rowmeshdict::Matrix{Int64}, r::DGfunction)
    colmesh = r[1]
    v = zeros(maximum(rowmeshdict))
    i = 1; j = 1
    while i*j >0 
        for lfe_i in localFEs(rowmeshdict,i)
            v[idx(rowmeshdict,lfe_i,i)] += integralL2H1_1D(colmesh[j], r[2][j], (rowmesh[i], lfe_i))           
        end
        i,j = next(rowmesh,colmesh,i,j)
    end

    return v
end
function addDiscontinuousGalerkinToVector!(v, rowmesh::Vector{DyadicIndexType}, rowmeshdict::Matrix{Int64}, r::DGfunction, c::Float64)
    colmesh = r[1]
    i = 1; j = 1
    while i*j >0 
        for lfe_i in localFEs(rowmeshdict,i)
            v[idx(rowmeshdict,lfe_i,i)] += c*integralL2H1_2D(colmesh[j], r[2][j][1], r[2][j][2], (rowmesh[i], lfe_i))           
        end
        i,j = next(rowmesh,colmesh,i,j)
    end
end

function genconstrhs(mesh::Mesh, meshdict::Matrix{Int64})
    v = zeros(maximum(meshdict))
    for (i,tile) in enumerate(mesh)
        for j in 1:2
            if meshdict[j,i] != 0 
                v[meshdict[j,i]] +=  0.5 * 2.0^(-tile[1]) 
            end
        end
    end 
    return v
end

function inclusion(meshold::Mesh, meshnew::Mesh, meshdictold::Matrix{Int64}, meshdictnew::Matrix{Int64}, u::Vector{Float64} )
    N = maximum(meshdictnew)
    unew = zeros(N)
    i = 1; j = 1
    while i*j >0
        V = vertexes(meshnew[i]) 
        for lfe_i in localFEs(meshdictnew,i)
            x = V[lfe_i+1]
            Λ  = bary(meshold[j], x)
            s = 0.0
            for (lfe_j, λ) in enumerate( Λ)
                idx = meshdictold[lfe_j, j]
                if idx != 0
                    s += λ * u[idx]
                end            
            end
            unew[idx(meshdictnew,lfe_i,i)] = s 
        end
        i,j = next(meshnew,meshold,i,j)
    end
    return unew
end