# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis

using Printf
using TimerOutputs
using StaticArrays
using Base.Threads
using DataStructures

include("Tree1D.jl")
include("LinFEM1D.jl")
include("AdaptFEM.jl")
include("GalerkinSolve.jl")
include("PointwiseSolver.jl")

Framelevel = 2
TileRefinement = false
GradeMesh = false
ℓ = 4


function Opinfo(α::Float64)
	c = 0.1 #constant for ellipticity
	Capprox = 0.02
	c_shifted = c*0.5^(0*α)
	return OperatorInfo(c_shifted, Capprox, α, IterHat1D)
end

##this function creates approximate solutions with decay in the coefficients 2^(α ℓ) and at most maxS tiles 

function driver1D(α::Float64, maxS::Int64 = 10^6)
	#___initialization of all parameters begin
	println("1D: maxS=", maxS, " α=", α)
	F = MultiIndexMap([ MultiIndex()])
	mesh = reduce(vcat, children.(StaticRoots(),1))
	S = [mesh]
	meshdicts = meshDict.(S)

	## roots for refining the triangulation
	## if refinement is done as described in the paper, then roots are finite element functions from the frame
	## However if we choose to refine using tiles, roots are tiles
	roots = StaticFiniteElementsRoots()
	if TileRefinement
		roots = StaticRoots()
	end

	leaves = [(i,r) for i in 1:length(S), r in roots][:]
	c_th = 0.1 #thresholding constant
	opinfo = Opinfo(α)
	u = zeros(S)
	uold = zeros(S)
	Fold = []
	Sold = []

	timed = []
	
	δ = 0.5 #
	rnorm = 0.1
	c2 = 0.0002
	res = zeros(S)
    res[1] = genconstrhs( S[1], meshdicts[1])
	#___initialization end
	to2 = TimerOutput()
	limit_not_reached = true
	stp = 0
	η0 = 1
	while limit_not_reached
		stp+=1
		@timeit to2 "running" begin
			η0 = min(δ/(1+δ)*rnorm, η0)
			ζ = δ/(1+δ)
			δu = zeros(S)
			solveGalerkinSystem1D!(opinfo, F, S, meshdicts, c2*rnorm, δu, res, ℓ)
			u.+= δu
			Fext, applied_approx ,BPXcoeffs, rerrbound, rnorm, η0 = resApprox(opinfo, F, S, meshdicts, u, ζ, η0) 
		end
		secs = round(TimerOutputs.time(to2["running"])/1e9)
		push!(timed, (rnorm + rerrbound,secs, length(F), sum(length(s) for s in S), F, S, u, leaves) )
		println("Step " , stp)
		@printf("Step: ε , sec, F , S}\n")
		for i in max(1, length(timed)-8) :length(timed)
			(t_r,t_s, tf, ts, F, S, u) = timed[i]
			@printf("%.4e , %d , %d, %d \n", t_r, t_s, tf, ts)
			if ts > maxS
				limit_not_reached = false
			end
		end

		@timeit to2 "running" begin
			Sold = deepcopy(S)
			Fold = deepcopy(F)
			uold = deepcopy(u)
			meshdictsold = deepcopy(meshdicts)
			if TileRefinement
				S, F, leaves  = getMeshFromBPXwithTileTree(Fext, BPXcoeffs, leaves, c_th)
			else
				S, F, leaves  = getMeshFromBPX(Fext, BPXcoeffs, leaves, c_th)
			end
			meshdicts = meshDict.(S)
			oldStochIndices = [F[f] for f in Fold.ν]
			StochIndices = [Fext[f] for f in F.ν]
			unew = inclusion.(Sold, S[oldStochIndices], meshdictsold, meshdicts[oldStochIndices], u)
			u = zeros(S)
			u[oldStochIndices] = unew
			applied_approx = applied_approx[StochIndices]  
			res = discontinuousGalerkinToVector.(S, meshdicts, applied_approx ).* (-1)
			i_0 = F[MultiIndex()]
			res[i_0] += genconstrhs( S[i_0], meshdicts[i_0])
		end
	end
	to = TimerOutput()
	return Fold, Sold, uold, timed
end

function thresholding(treesvals::Vector{Dict{T,Float64}}, oldroots :: Vector{Tuple{Int64,T}}, c_th::Float64) where T
	for t in treesvals
		list = StaticRoots()
		while !isempty(list)
			tile = pop!(list)
			for c in children(tile)
				if haskey(t,c)
					push!(list, c)
				end
			end
		end
	end
	if TileRefinement 
		static_roots = StaticRoots()
	else
		static_roots = StaticFiniteElementsRoots()
	end

	roots = deepcopy(oldroots)
	oldj = Set{Int64}([i for (i,node) in oldroots])
	for j in 1:length(treesvals)
		if j ∉ oldj
			append!(roots, [(j,root) for root in static_roots] )
		end
	end
	
	e((i,node)) =
		begin
			if i <= length(treesvals) && haskey(treesvals[i], node)
				return treesvals[i][node]
			else
				return 0.0
			end

		end
	errors_leaves = BinaryMinMaxHeap([(e(l),l) for l in roots])

	at_least_once = true
	sum_errors = sum(e(l) for l in roots)
	eps = (1-c_th^2)*sum_errors
	while at_least_once || sum_errors > eps
		at_least_once = false
		(max_error,(j,node)) = popmax!(errors_leaves)
		sum_errors -= e((j,node))
		for c in (TileRefinement ? children(node) : childrenframe(node)	)	
			e_c = e((j,c))
			sum_errors += e_c
			if e_c == 0
				new_tilde_e = 0
			else	
				new_tilde_e = 1.0/(  (max_error )^(-1) + e_c^(-1.0)  )
			end
			
			push!(errors_leaves, (new_tilde_e, (j,c) ) )
		end


	end
	return [l for (e,l) in popall!(errors_leaves)]
end

