# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients

This example code was used to generate the numerical tests in the paper
> M. Bachmayr, M. Eigel, H. Eisenmann and I. Voulis: A convergent adaptive finite element stochastic Galerkin method based on multilevel expansions of random fields, [arXiv:2403.13770](https://arxiv.org/abs/2403.13770)

## Installation

The required packages can be installed by running `include("1D/requirements_add.jl")` or `include("2D/requirements_add.jl")`, respectively.

## Usage

To get started, run the Jupyter notebooks `Demo.ipynb` in the respective subdirectories.

## Authors and acknowledgment

Example coded developed by Markus Bachmayr, Henrik Eisenmann and Igor Voulis.

## License

Released under MIT License

Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
