# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


# routines for solving Galerkin discretized problems
# on fixed index sets

include("StochResidual.jl")



function cgiter!(A::Function, M::Function, f::FunctionCoeff, u::FunctionCoeff, ϵ::Real, maxiter::Int64 = 500)
    #set size
    z = deepcopy(f)
    # set initial r
    r = deepcopy(f)
    A( u, -1., r)
    z = M(r)
    p = deepcopy(z)
    r0 = dot(z, r)
    println("cgiter started with r: ", sqrt(r0))
    Ap = zeros(p)
    i = 0

    while r0 > ϵ^2 && i < maxiter
        setzero(Ap)
        A( p, 1., Ap)
        α = r0 / dot(Ap, p) # 1 / in H^{-1} , H^1

        u .+= α .* p

        r .-= α*Ap
        z .= M(r)
        r1 = dot(z,r) # in H^1  , H^{-1}
        p .*= (r1 / r0)
        p .+= z
        r0 = r1
        i+=1
    end
    if r0 > ϵ^2
        println("Warning maxiter reached in cgiter, with residual ", sqrt(r0))
    end
    println("cgiter used ", i, " iterations")
    println("cgiter ended with r: ", sqrt(r0))
    return u, i, sqrt(r0)
end


function applyGalerkinMatrixfree2D!(F::MultiIndexMap, S::Vector{Mesh},
    meshdicts::Vector{Matrix{Int64}}, opinfo::OperatorInfo, level::Int64,
    v::FunctionCoeff, c::Real, w::FunctionCoeff)  #where T
    applySpatial2Dgal!(F, S, meshdicts, level , v, opinfo, w, c)
    return w
end



function solveGalerkinSystem2D!(opinfo::OperatorInfo, F::MultiIndexMap, S::Vector{Mesh}, 
                            meshdicts::Vector{Matrix{Int64}}, ϵ::Real, u::FunctionCoeff, f::FunctionCoeff, L::Int64)
    @printf("   [ entered solveGalerkinSystem: F %d, S %d \n", length(S), sum(length(s) for s in S))

    cnxData = CNXStruct2D[]
    for i in 1:length(S)
        cnxi = prepareCNX2D(S[i], meshdicts[i])
        push!(cnxData, cnxi)
    end
    precond(w::FunctionCoeff) = [cnxPrecondition2D(ww, cnx) for (ww, cnx) in zip(w, cnxData)]
    Op(x, c, y) = applyGalerkinMatrixfree2D!(F,  S, meshdicts, opinfo, L, x, c, y)
    i, normr = cgiter!(Op, precond, f, u, ϵ/2)
    return u
end

struct CNXStruct2D
    vertexList::Vector{VertexIndex2D}
    indexList::Vector{Int64}
    vertexIndices::Dict{VertexIndex2D,Int64}
end



function prepareCNX2D(mesh::Mesh, meshdict::Matrix{Int64})
    vertexList = Vector{VertexIndex2D}()
    indexList = Int64[]
    vertexIndices = Dict{VertexIndex2D,Int64}()
    for (i, tile) in enumerate(mesh)
        for (j, vertex) in enumerate(vertexes(tile))
            v = simplifyVertex(vertex)
            if meshdict[j,i] != 0 
                if !haskey(vertexIndices, v)
                    vertexIndices[v] = meshdict[j,i]
                    push!(vertexList,v)
                    push!(indexList, meshdict[j,i])
                end
            end
        end
    end
    permutation = sortperm(first.(vertexList), rev = true)
    vertexList = vertexList[permutation]
    return CNXStruct2D(vertexList,indexList[permutation], vertexIndices)
end

function cnxPrecondition2D(ww::Vector{Float64}, cnx::CNXStruct2D)
    w = deepcopy(ww)
    #since all triangles have angles 90,45,45 we already know the smoothers
    #this is special for 2D!
    Smoothers = ( 1/4 , [4 1; 1 4]/15 , [16  4 4; 4 15 1; 4 1 15]/56)
    increments = Vector{Float64}[]
    #"forward"
    for (index, vertex) in zip(cnx.indexList, cnx.vertexList)
        V = [index]
        for pp in getVertexParents(vertex)
            p = simplifyVertex(pp)
            if haskey(cnx.vertexIndices, p)
                push!(V, cnx.vertexIndices[p])
            end
        end
        push!(increments, Smoothers[length(V)]*w[V])
        if length(V)>1
            w[V[2:end]].+= 0.5 * w[index]
        end
    end
    Bw = 0*w
    #"backward"
    for (index, vertex) in zip(reverse(cnx.indexList), reverse(cnx.vertexList))
        V = [index] 
        for pp in getVertexParents(vertex)
            p = simplifyVertex(pp)
            if haskey(cnx.vertexIndices, p)
                push!(V, cnx.vertexIndices[p])
            end
        end
        Bw[index] = sum(Bw[V])/2
        Bw[V] += pop!(increments)
    end

    return Bw + ww/4
end

#End 2D

