# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


import Base.*
*(a::Float64,v::piecewisePoly) = (return Dict([k => a*p for (k,p) in v]))


function piecewisePolyToDiscontinuousGalerkin(pp::PiecewisePoly2D)
    roots = StaticRoots()
    tree = Set(roots)
    sizehint!(tree,2*length(pp))
    for t in roots
        if !haskey(pp, t)
            pp[t] = ((0.,0.,0.,0),(0.,0.,0.,0))
        end
    end
    tiles = deepcopy(keys(pp))
    #build tree and initialize sum as zero
    for tile in tiles 
        a = ancestor(tile)
        while !(a in tree) && !(tile in tree)
            push!(tree, a)
            if !haskey(pp,a)
                pp[a] = ((0.,0.,0.,0),(0.,0.,0.,0))
            end
            a = ancestor(a)  
        end
    end
    tiles = roots
    #add by iterating through all relevant tiles and through tree
    while length(tiles) > 0 
        for t in tiles
            hk = t in tree 
            if hk
                coeffs = pp[t]
                delete!(pp,t)
                for child in childrenTuple(t)    
                    b = (translateBilinear(coeffs[1], t, child) , translateBilinear(coeffs[2], t, child) )
                    if haskey(pp, child)
                        pp[child] = pp[child] +  b
                    else
                        pp[child] = b
                    end
                end
            end
        end
        newtiles = Vector{TriangleIndex2D}()
        for tile in tiles
            if tile in tree
                append!(newtiles, childrenTuple(tile))
            end
        end
        tiles = newtiles 
    end
    #use other format for output to match later algorithm
    tiling = Vector{TriangleIndex2D}()
    discontinuousg = Vector{BilinearGradInfo}()
    stack = reverse!(StaticRoots())
    while length(stack) > 0 
        tile = pop!(stack)
        if tile in tree
            append!(stack, reverse(childrenTuple(tile)))
        elseif haskey(pp, tile)
            push!(tiling, tile)
            push!(discontinuousg, pp[tile])
        end
    end
    return (tiling, discontinuousg)
end


function discontinuousGalerkinToVector(rowmesh::Vector{DyadicIndexType}, rowmeshdict::Matrix{Int64}, r::DGfunction2D)
    colmesh = r[1]
    v = zeros(maximum(rowmeshdict))
    i = 1; j = 1
    while i*j >0 
        for lfe_i in localFEs(rowmeshdict,i)
            v[idx(rowmeshdict,lfe_i,i)] += integralL2H1_2D(colmesh[j], r[2][j][1], r[2][j][2], (rowmesh[i], lfe_i))           
        end
        i,j = next(rowmesh,colmesh,i,j)
    end

    return v
end

function genconstrhs(mesh::Mesh, meshdict::Matrix{Int64})
    v = zeros(maximum(meshdict))
    for (i,tile) in enumerate(mesh)
        for j in 1:3
            if meshdict[j,i] != 0 
                if tile[4] == 0 || tile[4] == 1 
                    v[meshdict[j,i]] += 1/6 * 4.0^(-tile[1]) 
                else
                    v[meshdict[j,i]] += 1/12 * 4.0^(-tile[1])
                end
            end
        end
    end 
    return v
end

function inclusion(meshold::Mesh, meshnew::Mesh, meshdictold::Matrix{Int64}, meshdictnew::Matrix{Int64}, u::Vector{Float64} )
    N = maximum(meshdictnew)
    unew = zeros(N)
    i = 1; j = 1
    while i*j >0
        V = vertexes(meshnew[i]) 
        for lfe_i in localFEs(meshdictnew,i)
            x = coords(V[lfe_i+1])
            Λ  = bary(meshold[j], x)
            s = 0.0
            for (lfe_j, λ) in enumerate( Λ)
                idx = meshdictold[lfe_j, j]
                if idx != 0
                    s += λ * u[idx]
                end            
            end
            unew[idx(meshdictnew,lfe_i,i)] = s 
        end
        i,j = next(meshnew,meshold,i,j)
    end
    return unew
end