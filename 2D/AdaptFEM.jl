# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis

#=
Relies on the funktions and types from tree.jl
Also relies on the functions:
- val 
- val∂
- localDGDim
- localDGfe
- localFEs
- idx
- localRep
- quadPoints
=#
using SparseArrays

function meshDict(mesh::Mesh) #deterministic indexing of dofs on an (ordered) mesh, based on the first tile and the first node in this tile that sees this dof 
    idx = 1
    meshdict = fill(0,length(LocalFEs),length(mesh))
    dict = Dict{VertexIndexType, Int64}()
    for (j,tile) in enumerate(mesh)
        for (i,node) in enumerate(simplifyVertex.(vertexes(tile)))
            if !onBoundary(node)
                if !haskey(dict, node)
                    # we have a new degree of freedom
                    dict[node] = idx
                    meshdict[i,j] = idx
                    idx += 1
                else
                    #we already met the degree of freedom in this node and gave it index dict[node]
                    meshdict[i,j] = dict[node]
                end
            end
        end
    end
    return meshdict
end

function idx(meshdict::MeshDict, fe::LocalFE, i::Int64) #idx starting with 1
	return meshdict[fe+1,i]
end
function localFEs(meshdict::Matrix{Int64}, i::Int64)
	LocalFEs[meshdict[:,i].>0] #pick out the FEs that are active
end
function dimFE(meshdict::MeshDict)
	return maximum(meshdict)
end


function integralL2H1_2D(tile::TriangleIndex2D, ux::NTuple{4,Float64}, uy::NTuple{4,Float64}, vloc::Tuple{TriangleIndex2D, Int64})
    if intersect(tile, vloc[1])
        areas = area(vloc[1]), area(tile)
        tmp = argmin(areas)
        smallarea = areas[tmp]
        smalltile = (vloc[1], tile)[tmp]
        X = quadraturePoints(smalltile)
        square = (tile[1],tile[2],tile[3])
        uxweighted = sum(evalBilinear(square, ux, X))
        uyweighted = sum(evalBilinear(square, uy, X))
        g = val∂(vloc[1],vloc[2])
        return (g[1]*uxweighted + g[2]*uyweighted) * smallarea *1/3
    end
    return 0.0
end


#vloc is always finer than tile
function integralL2H1_2D__fastforbpx(tile::TriangleIndex2D, ux::NTuple{4,Float64}, uy::NTuple{4,Float64}, vloc::Tuple{TriangleIndex2D, Int64})
    smallarea = area(vloc[1])
    smalltile = vloc[1]
    X = quadraturePoints(smalltile)
    square = (tile[1],tile[2],tile[3])
    uxweighted = sum(evalBilinear(square, ux, X))
    uyweighted = sum(evalBilinear(square, uy, X))
    g = val∂(vloc[1],vloc[2])
    return (g[1]*uxweighted + g[2]*uyweighted) * smallarea *1/3
end




function next(mesh::Vector{DyadicIndexType}, i::Int64, bound::Int64)
    return i==bound ? 0 : i+1
end

function next(mesh::Vector{DyadicIndexType}, i::Int64)
    return next(mesh, i, length(mesh))
end

function next(meshi::Vector{DyadicIndexType}, meshj::Vector{DyadicIndexType}, i::Int64 , j::Int64, boundi::Int64, boundj::Int64)
    @assert intersect(meshi[i],meshj[j])
    if j<boundj && intersect(meshi[i],meshj[j+1])
        return i,j+1
    elseif i<boundi && intersect(meshi[i+1],meshj[j])
        return i+1,j
    else
        return next(meshi, i, boundi),next(meshj, j, boundj)
    end
end

function next(meshi::Vector{DyadicIndexType}, meshj::Vector{DyadicIndexType}, i::Int64 , j::Int64)
    return next(meshi, meshj, i, j, length(meshi), length(meshj))
end

function generateVector(mesh::Vector{DyadicIndexType}, f::Function)
    meshdict = meshDict(mesh)
    I = Int64[]; J = Int64[]; V = Float64[];
    i = 1
    while i >0 
        for lfe_i in localFEs(meshdict,i)
            push!(I, idx(meshdict,lfe_i,i) )
            push!(J, 1 )
            push!(V, f( (mesh[i], lfe_i) ))
        end
        i = next(mesh,i)
    end
    return vec(sparse(I, J, V))
end

#for 2D! 
function normH1(mesh::Mesh, meshdict::Matrix{Int64},u::Vector{Float64})
    s =0.0
    for i in 1:length(mesh)
        @views idx1, idx2, idx3 = meshdict[:, i]
        a = idx1 == 0 ? 0.0 : u[idx1]
        b = idx2 == 0 ? 0.0 : u[idx2]
        c = idx3 == 0 ? 0.0 : u[idx3]
        s += (a-b)^2 + (a-c)^2
    end
    return sqrt(s)*sqrt(0.5)
end





