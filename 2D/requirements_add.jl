using Pkg
Pkg.add("TimerOutputs")
Pkg.add("StaticArrays")
Pkg.add("DataStructures")
Pkg.add("Plots") #optional
Pkg.add("LegendrePolynomials") #optional