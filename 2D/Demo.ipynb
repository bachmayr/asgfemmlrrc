{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3>Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients</h3>\n",
    "Released under MIT License, see <a href=\"https://git.rwth-aachen.de/bachmayr/asgfemmlrrc/-/blob/22af3f3c5089f492f83dbfcc4517f07492620236/README.md\">README.md</a><br/>\n",
    "Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our aim is to approximate the solution of the model problem\n",
    "$$\n",
    "    -\\nabla_x \\cdot(a(x,y) \\nabla_x u(x,y)) = f(x) \\quad\\text{for $x\\in D$, }\\\\\n",
    "    u(x,y) = 0 \\quad\\text{for $x\\in \\partial D,$ }\n",
    "$$\n",
    "where $D$ is an L-shaped domain and\n",
    "$$\n",
    "    a(x,y) = \\theta_0(x)  +  \\sum_\\mu y_\\mu\\theta_\\mu(x)\n",
    "$$\n",
    "is a multilevel representation of a random field.\n",
    "\n",
    "We first include the necessary files for approximating the solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "include(\"AdaptiveSolver.jl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `driver2D` takes as input $\\alpha$ representing the decay in the diffusion coefficient and `N` as a stopping criterium for the size $\\mathcal N(\\mathbb T)$. It computes an approximation to the solution of the model problem with constant right-hand side.\n",
    "\n",
    "\n",
    "The output `F, S, u, timed` contains the required polynomial multiindices in `F`, the family of triangulations in `S`, the coefficients of the solution in `u` and data concerning the error, degrees of freedom, and computational time, as well as intermediate solutions in `timed`.\n",
    "\n",
    "<span style=\"color:red\"> Attention! </span> This can be demanding on memory for larger `N`!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "α = 2.0/3\n",
    "F, S, u, timed  = driver2D(α,100000)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now load files for visualization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "include(\"PresentationTools.jl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A convergence plot can be shown with the function `conv_plot1d` with the additional information of `timed` and $\\alpha$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#convergence plot\n",
    "conv_plot2d(timed, α)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `piecewiselinearplot` plots a function given by its conforming triangulation and nodal coefficients. In our case, these are the entries of `S[i]` and `u[i]`.\n",
    "\n",
    "\n",
    "<span style=\"color:red\"> Attention! </span> This can be demanding on memory for medium sized `N`!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#function coefficients of different polynomials\n",
    "n = min(length(S),9)\n",
    "p = Array{Plots.Plot{Plots.GRBackend}, 1}(undef, n)\n",
    "\n",
    "for i in 1:n\n",
    "    p[i] = piecewiselinearplot(S[i], u[i])\n",
    "end\n",
    "plot(p... , layout = (3, floor(Int(n/3))), size = (700, 700) )\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can evaluate our solution at any given point $y$. The file `PointwiseSolver.jl` contains functions for solving\n",
    "the model problem\n",
    "for a given $y$ and a given grid. We can thus compare the approximate solution `u` at any point to a pointwise solution on a finer grid.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "include(\"PointwiseSolver.jl\")\n",
    "ℓ = 7 \n",
    "N = (2^ℓ-1)^2\n",
    "y = rand(Float64,N)*2 .- 1.0\n",
    "\n",
    "u_in_y, mesh = stocheval(F, S, u, y)\n",
    "finemesh = children(mesh,2)\n",
    "@time u_y_gal = PointwiseSolver2D(α, finemesh, [y], zeros([finemesh]),1e-8)[1]   \n",
    "   \n",
    "\n",
    "diff_to_mean, mesh = sum_u([1.0,-1.0],[u_in_y,u[1]],[mesh, S[1]])\n",
    "diff_to_mean2, finemesh = sum_u([1.0,-1.0],[u_y_gal,u[1]],[finemesh, S[1]])\n",
    "diff_to_sol, finemesh = sum_u([1.0,-1.0],[u_y_gal,u_in_y],[finemesh, mesh])\n",
    "\n",
    "println(\"H1-norm of difference of solutions = \", normH1(finemesh , meshDict(finemesh) , diff_to_sol))\n",
    "\n",
    "piecewiselinearplot(finemesh, diff_to_mean2, colorbaropt = true)\n",
    "display(plot!(title = \"Direct computation - diff to mean\"))\n",
    "piecewiselinearplot(mesh, diff_to_mean, colorbaropt = true)\n",
    "display(plot!(title = \"Evaluation of approximation - diff to mean\"))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use this functionality to estimate the $L_2(Y)\\times H_0^1$-error by sampling. \n",
    "<span style=\"color:red\"> Attention! </span>  This will take several hours.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#MonteCarloScript\n",
    "include(\"PresentationTools.jl\")\n",
    "include(\"PointwiseSolver.jl\")\n",
    "start = 2\n",
    "\n",
    "timedMC = timed[1:start-1]\n",
    "for i in start:length(timed)\n",
    "    F = timed[i][5]\n",
    "    S = timed[i][6]\n",
    "    u = timed[i][7]\n",
    "    est, errors = MonteCarloEstimator(F, S, u, α)\n",
    "    push!(timedMC, (est, timed[i][2], timed[i][3], timed[i][4], timed[i][5], timed[i][6], timed[i][7] ))\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The observed rates using MonteCarlo sampling closely match the ones produced by the error estimation using finite element frames:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "conv_plot2d(timedMC, 3/4,  L\"rate $s=\\frac{3}{4}$\", L\"d = 1, \\alpha = \\frac{2}{3}\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.9.1",
   "language": "julia",
   "name": "julia-1.9"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.9.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
