# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


# auxiliary definitions and routines
# for defining and handling random PDEs
using LinearAlgebra, SparseArrays

import Base: zeros, iterate, length, eltype, +
import LinearAlgebra: norm, dot

struct OperatorInfo
    c::Float64
    Capprox::Float64
    α::Float64
    μiter::Type # subtype of OperatorIterable
end


const FunctionCoeff = Vector{Vector{Float64}}

const BilinearGradInfo = NTuple{2,NTuple{4, Float64}}
Base.:+(a::BilinearGradInfo, b::BilinearGradInfo) = (a[1].+b[1], a[2].+b[2])
const PiecewisePoly2D = Dict{TriangleIndex2D, BilinearGradInfo}

const DGfunction2D = Tuple{Mesh,Vector{BilinearGradInfo}}






function zeros(u::FunctionCoeff)
    return [ zeros(length(u[i])) for i in eachindex(u) ]
end

function zeros(meshes::Vector{Mesh} )
    return [ zeros(dimFE(meshDict(mesh))) for mesh in meshes ]
end

function setzero(u::FunctionCoeff)
    broadcast(x->(x.=0.0), u)
end

function sqrtbeta(k::Integer)
    return 1.0/sqrt(4.0 - 1.0/k^2)
end


abstract type OperatorIterable end

function scaleFactor(opinfo::OperatorInfo, level::Int64)
    if  level ≥ 0
        return opinfo.c*2^(-opinfo.α* level)
    else
        return 1.
    end
end


function suppHat1D(μ::Int64) #should return a sorted submesh!
    j = μ == 0 ? 0 : floor(Int64, log2(μ))
    k = μ == 0 ? 0 : μ - 2^j
    # dyadic tile of support, level of further subdivision:
    #return [DyadicIndex{1}(j,2^j - k - 1)], 1
    return [(j,k)], 1
end

struct IterHat1D <: OperatorIterable
    maxμ::Int64
    function IterHat1D(L::Int64)
        if L >= 0
            maxμ = 2^(L+1) - 1
        else
            maxμ = 0
        end
        return new(maxμ)
    end
end

# return tuple of first iterate and initial state
function Base.iterate(iter::IterHat1D)
    if iter.maxμ > 0
        return (1, 1)
    else
        return nothing
    end
end

# returns tuple of next iterate and next state
function Base.iterate(iter::IterHat1D, state::Int64)
    if state < iter.maxμ
        return (state+1, state+1)
    else
        return nothing
    end
end

Base.length(iter::IterHat1D) = iter.maxμ
Base.eltype(iter::IterHat1D) = Int64




# iterate over subdivision of a tile defined by a DyadicIndex{1}
const DyadicIndex1D = Tuple{Int64,Int64}

struct SupportTile1D
    lower::Float64
    upper::Float64
    tile::DyadicIndex1D
    index::Int64
end

struct SupportTilesIter1D
    tile::DyadicIndex1D
    imax::Int64

    function SupportTilesIter1D(d::DyadicIndex1D, subdivlevel::Int64)
        @assert subdivlevel >= 0
        return new(children(d,subdivlevel)[1], 2^subdivlevel)
    end
end

function Base.iterate(iter::SupportTilesIter1D)
    return ( SupportTile1D(getDomain(iter.tile)..., iter.tile, 1), 1)
end

function Base.iterate(iter::SupportTilesIter1D, state::Int64)
    if state < iter.imax
        nextTile = geometric_next(iter.tile)
        return ( SupportTile1D(getDomain(nextTile)..., nextTile, state+1), state+1)
    else
        return nothing
    end
end

Base.length(iter::SupportTilesIter1D) = iter.imax
Base.eltype(iter::SupportTilesIter1D) = SupportTile1D


function getLevel2D(μ::Int64)
    floor(Int64,log2(sqrt(μ-1)+1)+1 )
end

function get1DtopInverse(idx::DyadicIndex1D)
    level = idx[1]
    beforethislevel = 2^(level-1)-1
    return beforethislevel + idx[2]÷2 + 1
end

function get2DlevelRange(l::Int64)
    if l == -1
        return 0:0
    end
    return (2^(l)-1)^2+1   :   (2^(l+1)-1)^2
end

function get2DtopInverse(idx::VertexIndex2D)
    k = 0
    level = idx[1]
    beforethislevel = (2^(level-1) - 1)^2
    beforethislevel1D = (2^(level-1) - 1)
    if (idx[2]+idx[3])%2 == 0
        k = ((idx[2]-1) ÷ 2) * 2^(level - 1) + ((idx[3]-1) ÷ 2) + 1
        return beforethislevel + k
    elseif idx[3]%2 == 0
        top1d_l = level 
        top1d_k = idx[3]
        while top1d_k % 2 == 0
            top1d_l -= 1 
            top1d_k ÷= 2
        end
        μ1d = get1DtopInverse((top1d_l,top1d_k))
        k = 4^(level-1) + (idx[2]÷2) * beforethislevel1D +μ1d
        return beforethislevel + k 
    elseif idx[2]%2 == 0
        top1d_l = level 
        top1d_k = idx[2]
        while top1d_k % 2 == 0
            top1d_l -= 1 
            top1d_k ÷= 2
        end
        μ1d = get1DtopInverse((top1d_l,top1d_k))
        k = 4^(level-1) + beforethislevel1D * (beforethislevel1D+1) + (idx[3]÷2) * beforethislevel1D + μ1d
        return beforethislevel + k 

    end#else idx not valid
    return k
end

function get2DtopFromSquare(square::SquareIndex2D)
    v1,v2,v3 = (0,0,0)
    for v in getSquareVertices(square)
        if v == simplifyVertex(v) &&  inDomain(v) 
            if v1 == 0
                v1 = v
            elseif v2 == 0
                v2 = v
            else
                v3 = v
                return v1,v2,v3
            end
        end
    end
    if v1 == 0
        return 0:-1
    elseif v2 == 0
        return (v1,) 
    end
    return v1,v2
end

function bilinearFromSquareAnd2Dtop(square::SquareIndex2D, idx::VertexIndex2D)
    return Float64.(getSquareVertices(square).==(idx,idx,idx,idx))

end



# iterating over all μ up to a certain level:

struct IterHat2D <: OperatorIterable# where d::Int64
    maxμ::Int64
    function IterHat2D(L::Int64)
        if L >= 0
            maxμ = (2^(L+1) - 1)^2
        else
            maxμ = 0
        end
        return new(maxμ)
    end
end

# return tuple of first iterate and initial state
function Base.iterate(iter::IterHat2D)
    if iter.maxμ > 0
        return (1, 1)
    else
        return nothing
    end
end

# returns tuple of next iterate and next state
function Base.iterate(iter::IterHat2D, state::Int64)
    if state < iter.maxμ
        return (state+1, state+1)
    else
        return nothing
    end
end

Base.length(iter::IterHat2D) = iter.maxμ
Base.eltype(iter::IterHat2D) = Int64


# iterate over subdivision of a tile defined by a DyadicIndex{2}







function applyHatLevel!(level::Int64, u::Vector{Float64},  PPs::Vector{PiecewisePoly2D}, mesh::Mesh, meshdict::Matrix{Int64}, 
                    indices::Matrix{Int64} , coeffs::Vector{Float64} = ones(length(PPs)))
    #indices tells us where to add to
    if  level == -1
        idx = indices[1,1]
        c = coeffs[idx]
        #go through mesh
        for (i,tile) in enumerate(mesh)
            g = (0., 0.)
            for j in 1:3
                if meshdict[j,i] != 0 
                    g = g .+ ((u[meshdict[j,i]]*c) .* val∂(tile, j-1))
                end
            end
            if haskey(PPs[idx], tile)
                PPs[idx][tile] += ((g[1], g[1], g[1], g[1]),(g[2], g[2], g[2], g[2]))
            else
                PPs[idx][tile] = ((g[1], g[1], g[1], g[1]),(g[2], g[2], g[2], g[2]))
            end
        end
    else
        beforethislevel = (2^(level)-1)^2
        for (i,tile) in enumerate(mesh)
            if tile[1] ≥ level + 1
                square = (level+1, tile[2]÷ 2^(tile[1] - level-1)  , tile[3]÷ 2^(tile[1] - level-1))
                for top in get2DtopFromSquare(square)
                    μ = get2DtopInverse(top) 
                    @views idx1, idx2 = indices[:, μ - beforethislevel]
                    if idx1 != 0 || idx2 != 0
                        c1 = idx1 == 0 ? 0 : coeffs[idx1]
                        c2 = idx2 == 0 ? 0 : coeffs[idx2]
                        b = bilinearFromSquareAnd2Dtop(square, top)
                        b = translateBilinear(b, square, tile[1:3])
                        g = (0., 0.)
                        for j in 1:3
                            if meshdict[j,i] != 0 
                                g = g .+ ((u[meshdict[j,i]]) .* val∂(tile, j-1))
                            end
                        end
                        if idx1 != 0
                            if haskey(PPs[idx1], tile)
                                PPs[idx1][tile] += (g[1].*b.*c1,g[2].*b.*c1)
                            else
                                PPs[idx1][tile] = (g[1].*b.*c1,g[2].*b.*c1)
                            end
                        end
                        if idx2 != 0 
                            if haskey(PPs[idx2], tile)
                                PPs[idx2][tile] += (g[1].*b.*c2,g[2].*b.*c2)
                            else
                                PPs[idx2][tile] = (g[1].*b.*c2,g[2].*b.*c2)
                            end
                        end
                    end
                end
            else
                childs = children(tile,2*(level+1 - tile[1]) - (tile[4]>1))
                for kid in childs
                    @views square = kid[1:3]
                    for top in get2DtopFromSquare(square)
                        μ = get2DtopInverse(top)
                        @views idx1, idx2 = indices[:, μ - beforethislevel]
                        if idx1 != 0 || idx2 != 0
                            c1 = idx1 != 0  ? coeffs[idx1] : 0.0
                            c2 = idx2 != 0 ? coeffs[idx2] : 0.0
                            b = bilinearFromSquareAnd2Dtop(square, top)
                            g = (0., 0.)
                            for j in 1:3
                                if meshdict[j,i] != 0 
                                    g = g .+ ((u[meshdict[j,i]]) .* val∂(tile, j-1))
                                end
                            end
                            if idx1 != 0
                                if haskey(PPs[idx1], kid)
                                    PPs[idx1][kid] += (g[1].*b.*c1,g[2].*b.*c1)
                                else
                                    PPs[idx1][kid] = (g[1].*b.*c1,g[2].*b.*c1)
                                end
                            end
                            if idx2 != 0 
                                if haskey(PPs[idx2], kid)
                                    PPs[idx2][kid] += (g[1].*b.*c2,g[2].*b.*c2)
                                else
                                    PPs[idx2][kid] = (g[1].*b.*c2,g[2].*b.*c2)
                                end
                            end
                        end
                    end
                end
            end
        end

    end
end



