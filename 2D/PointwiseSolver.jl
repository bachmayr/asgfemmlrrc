# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis



function PointwiseSolver2D(α::Float64, mesh::Mesh, ys::Vector{Vector{Float64}}, u::FunctionCoeff, ϵ::Float64)
	c = 0.1

	Capprox = 0.02
	c_shifted = c*0.5^(-LSHAPED*α)
	opinfo = OperatorInfo(c_shifted, Capprox, α, IterHat2D)
    
    @printf("   [ entered solveGalerkinSystem")
    meshdict = meshDict(mesh)

    cnxData =  prepareCNX2D(mesh, meshdict)


    precond(w::FunctionCoeff) = [cnxPrecondition2D(ww, cnxData) for ww in w]
    Op(x, c, w) = [applySpatial2DgalPointwise!(mesh, meshdict, y , xx, opinfo, ww, c) for (xx,ww,y) in zip(x,w,ys)]

    f = [genconstrhs( mesh, meshdict) for y in ys]
    i, normr = cgiter!(Op, precond, f, u, ϵ)
    return u
end



# for MonteCarlo
# coeffs needs to be of length 2^j - 1 for some j
function applySpatial2DPointwise(mesh::Mesh, meshdict::Matrix{Int64}, y::Vector{Float64} , u::Vector{Float64}, opinfo::OperatorInfo)
    PP = PiecewisePoly2D()
    level  =  Int(log2(sqrt(length(y))+1)) - 1
    
    for l in -1:level
        scale = scaleFactor(opinfo, l)
        indexmap = zeros(Int64, 2 , length(get2DlevelRange(l)))
        if l ≥ 0 
            beforethislevel = first(get2DlevelRange(l))-1
            for μ in get2DlevelRange(l)
                indexmap[1, μ - beforethislevel] = μ - beforethislevel
            end
        else
            indexmap[1,1] = 1
        end
        applyHatLevel!(l, u , [PP for i in get2DlevelRange(l)] , mesh, meshdict, indexmap, l == -1 ? [1.0] : scale* y[get2DlevelRange(l)])
    end
    DG = piecewisePolyToDiscontinuousGalerkin(PP) 
    
    return DG
end



# for CG method
function applySpatial2DgalPointwise!(mesh::Mesh, meshdict::Matrix{Int64}, y::Vector{Float64} , u::Vector{Float64}, opinfo::OperatorInfo, w::Vector{Float64}, c::Float64)
    PP = PiecewisePoly2D()
    level  =  Int(log2(sqrt(length(y))+1)) - 1
    for l in -1:level
        scale = scaleFactor(opinfo, l)

        indexmap = zeros(Int64, 2 , length(get2DlevelRange(l)))
        if l ≥ 0 
            beforethislevel = first(get2DlevelRange(l))-1
            for μ in get2DlevelRange(l)
                indexmap[1, μ - beforethislevel] = μ - beforethislevel
            end
        else
            indexmap[1,1] = 1
        end
        magicfactor = 1
        applyHatLevel!(l, u , [PP for i in get2DlevelRange(l)] , mesh, meshdict, indexmap, l == -1 ? [1.0] : magicfactor*scale*y[get2DlevelRange(l)])
    end
    DG = piecewisePolyToDiscontinuousGalerkin(PP)
    w .+= c* discontinuousGalerkinToVector(mesh, meshdict, DG)

    return w
end


